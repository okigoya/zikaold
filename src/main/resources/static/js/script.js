

$(document).ready(function () {
     const osmLayer = new ol.layer.Tile({
            source: new ol.source.OSM(),
            visible: true
        });

        const map = new ol.Map({
            target: 'mapPlaceholder',
            layers: [
                osmLayer
            ],
            view: new ol.View({
                center: ol.proj.fromLonLat([260.55, 23]),
                zoom: 4
            })
        });

        const pointStyleBlue = new ol.style.Style({
            image: new ol.style.Circle({
                radius: 3,
                 fill: new ol.style.Fill({color: 'blue' }),
                stroke: new ol.style.Stroke({color: 'white', width: 1})
            })
        });
         const pointStyleYellow = new ol.style.Style({
                    image: new ol.style.Circle({
                        radius: 10,
                         fill: new ol.style.Fill({color: 'gold' }),
                        stroke: new ol.style.Stroke({color: 'white', width: 1})
                    })
                });

        const pointStyleOrange = new ol.style.Style({
            image: new ol.style.Circle({
                radius: 15,
                 fill: new ol.style.Fill({color: 'orange' }),
                stroke: new ol.style.Stroke({color: 'white', width: 1})
            })
        });

        const pointStyleRed = new ol.style.Style({
            image: new ol.style.Circle({
                radius: 20,
                fill: new ol.style.Fill({color: 'red' }),
                stroke: new ol.style.Stroke({color: 'white', width: 1})
            })
        });

        const pointStyleRedX = new ol.style.Style({
            image: new ol.style.Circle({
                radius: 25,
                fill: new ol.style.Fill({color: 'red' }),
                stroke: new ol.style.Stroke({color: 'white', width: 1})
            })
        });


      const reportSource = new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://localhost:8080/report/'
        });
        const reportLayer = new ol.layer.Vector({
            source: reportSource,
            style: function(feature, resolution) {
                const value = feature.get('value');
                if (value < 10 ) {
                    return pointStyleBlue;}
                else if (value > 10 && value < 30) {
                    return pointStyleYellow;
                }
                else if (value > 30 && value < 50) {
                    return pointStyleOrange;
                }
                else if (value > 50 && value < 100) {
                    return pointStyleRed;
                }
                else if (value > 100) {
                     return pointStyleRedX;
                 }
            }
        });
        map.addLayer(reportLayer);

    map.on('click', function(event) {
        $('#reports').empty();
        let loc = '';
        map.forEachFeatureAtPixel(event.pixel, function(feature,layer) {
        let locHd='';
           if (loc !== feature.get('location')) {
                locHd = `<h3>${feature.get('location')}</h3>`;
            }

            $('#reports').append(
         ` ${locHd}
            <p>Report Date: ${feature.get('reportDate')}<br />
            Reported: ${feature.get('dataField')} -  ${feature.get('value')} ${feature.get('unit')}</p>`
            );

            loc = feature.get('location');

        });
    })

});