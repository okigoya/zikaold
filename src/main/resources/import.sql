BEGIN;

CREATE EXTENSION IF NOT EXISTS postgis;
CREATE EXTENSION IF NOT EXISTS postgis_topology;
CREATE EXTENSION IF NOT EXISTS fuzzystrmatch;
CREATE EXTENSION IF NOT EXISTS postgis_tiger_geocoder;
CREATE EXTENSION IF NOT EXISTS unaccent;

-- COPY report() FROM '/tmp/report.csv' DELIMITER ',' CSV HEADER;
COPY report(report_date, location, location_type, data_field, data_field_code, time_period, time_period_type, value, unit, location_geometry) from '/tmp/Brazil_Zika-2016-04-02.csv' DELIMITER ',' CSV HEADER;
COPY report(report_date,location,location_type, data_field, data_field_code,time_period,time_period_type, value, unit) from '/tmp/El_Salvador-2016-02-20.csv' DELIMITER ',' CSV HEADER;
COPY report(report_date, location, location_type, data_field, data_field_code, time_period, time_period_type, value, unit, location_geometry) from '/tmp/Haiti_Zika-2016-02-03.csv' DELIMITER ',' CSV HEADER;
COPY report(report_date, location, location_type, data_field, data_field_code, time_period, time_period_type, value, unit, location_geometry) from '/tmp/Mexico_Zika-2016-02-20.csv' DELIMITER ',' CSV HEADER;
COPY report(report_date, location, location_type, data_field, data_field_code, time_period, time_period_type, value, unit) from '/tmp/MINSA_ZIKA_Search-Week_08.csv' DELIMITER ',' CSV HEADER;
COPY report(report_date, location, location_type, data_field, data_field_code, time_period, time_period_type, value, unit, location_geometry) from '/tmp/Panama_Zika-2016-02-18.csv' DELIMITER ',' CSV HEADER;
--
--UPDATE report SET location_geometry='POINT(-88.74940 13.86483)' WHERE location = 'El_Salvador-Cabanas' AND location_geometry IS NULL;
--UPDATE report SET location_geometry='POINT(-86.86469 12.52023)' WHERE location = 'Nicaragua-Leon-Telica' AND location_geometry IS NULL;
--UPDATE report SET location_geometry='POINT(-88.97968 13.49536)' WHERE location = 'El_Salvador-La_Paz' AND location_geometry IS NULL;
--UPDATE report SET location_geometry='POINT(-90.48063 14.57510)' WHERE location = 'El_Salvador-Guatemala' AND location_geometry IS NULL;
--UPDATE report SET location_geometry='POINT(-89.21819 13.69294)' WHERE location = 'El_Salvador-San_Salvador' AND location_geometry IS NULL;
--UPDATE report SET location_geometry='POINT(-88.78306 13.63910)' WHERE location = 'El_Salvador-San_Vicente' AND location_geometry IS NULL;
--UPDATE report SET location_geometry='POINT(-85.20723 12.86542)' WHERE location = 'Nicaragua' AND location_geometry IS NULL;
--UPDATE report SET location_geometry='POINT(-88.12914 13.76820)' WHERE location = 'El_Salvador-Morazan' AND location_geometry IS NULL;
--UPDATE report SET location_geometry='POINT(-88.89653 13.79419)' WHERE location = 'El_Salvador' AND location_geometry IS NULL;
--UPDATE report SET location_geometry='POINT(-88.44281 13.34531)' WHERE location = 'El_Salvador-Usulutan' AND location_geometry IS NULL;
--UPDATE report SET location_geometry='POINT(-89.72861 13.70997)' WHERE location = 'El_Salvador-Sonsonate' AND location_geometry IS NULL;
--UPDATE report SET location_geometry='POINT(-87.85006 13.33235)' WHERE location = 'El_Salvador-La_Union' AND location_geometry IS NULL;
--UPDATE report SET location_geometry='POINT(-86.30217 12.15582)' WHERE location = 'Nicaragua-Managua-Distrito_II' AND location_geometry IS NULL;
--UPDATE report SET location_geometry='POINT(-88.91264 14.0164)' WHERE location = 'El_Salvador-Chalatenango' AND location_geometry IS NULL;
--UPDATE report SET location_geometry='POINT(-89.30846 13.49070)' WHERE location = 'El_Salvador-La_Libertad' AND location_geometry IS NULL;
--UPDATE report SET location_geometry='POINT(-89.56391 13.97783)' WHERE location = 'El_Salvador-Santa_Ana' AND location_geometry IS NULL;
--UPDATE report SET location_geometry='POINT(-88.17792 13.47929)' WHERE location = 'El_Salvador-San_Miguel' AND location_geometry IS NULL;
--UPDATE report SET location_geometry='POINT(-89.84114 13.92595)' WHERE location = 'El_Salvador-Ahuachapan' AND location_geometry IS NULL;
--UPDATE report SET location_geometry='POINT(-89.05615 13.86620)' WHERE location = '' AND location_geometry IS NULL;
--UPDATE report SET location_geometry='POINT(-89.05615 13.86620)' WHERE location = 'El_Salvador-Cuscatlan ' AND location_geometry IS NULL;

COMMIT;