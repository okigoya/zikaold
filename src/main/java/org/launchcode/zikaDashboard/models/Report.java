package org.launchcode.zikaDashboard.models;

import com.vividsolutions.jts.geom.Geometry;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
    public class Report {

        private static final long serialVersionUID = 1L;

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

        private String reportDate;
        private String location;
        private Geometry locationGeometry;
        private String locationType;
        private String dataField;
        private String dataFieldCode;
        private String time_period;
        private String time_period_type;
        private Integer value;
        private String unit;
        // required Hibernate constructor
        public Report() {}

        public Report(String reportDate,
                      Geometry locationGeometry,
                      String location,
                      String locationType,
                      String dataField,
                      String dataFieldCode,
                      String time_period,
                      String time_period_type,
                      Integer value,
                      String unit) {
            this.reportDate=reportDate;
            this.location=location;
            this.locationGeometry=locationGeometry;
            this.locationType=locationType;
            this.dataField=dataField;
            this.dataFieldCode=dataFieldCode;
            this.time_period=time_period;
            this.time_period_type=time_period_type;
            this.value=value;
            this.unit=unit;
        }

    public Long getId() {
        return id;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getLocation(){
            return location;
    }
    public void setLocation(String location) {
            this.location = location;

    }
    public Geometry getLocationGeometry() {
        return locationGeometry;
    }

    public void setLocationGeometry(Geometry locationGeometry) {
        this.locationGeometry = locationGeometry;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getDataField() {
        return dataField;
    }

    public void setDataField(String dataField) {
        this.dataField = dataField;
    }

    public String getDataFieldCode() {
        return dataFieldCode;
    }

    public void setDataFieldCode(String dataFieldCode) {
        this.dataFieldCode = dataFieldCode;
    }

    public String getTime_period() {
        return time_period;
    }

    public void setTime_period(String time_period) {
        this.time_period = time_period;
    }

    public String getTime_period_type() {
        return time_period_type;
    }

    public void setTime_period_type(String time_period_type) {
        this.time_period_type = time_period_type;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
