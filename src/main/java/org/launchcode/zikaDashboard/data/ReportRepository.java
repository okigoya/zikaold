package org.launchcode.zikaDashboard.data;

import org.launchcode.zikaDashboard.models.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;


@Transactional
@Repository
public interface ReportRepository extends JpaRepository<Report, Integer> {

//    @Query(value="select report_date, location, location_type, data_field, location_geometry, unit, sum(value) as value from report group by report_date, location, location_type, data_field, location_geometry, unit",
//            nativeQuery = true)
//    List<Report> getReport();

}
